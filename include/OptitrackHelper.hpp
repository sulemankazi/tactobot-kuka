/*
 * OptitrackHelper.hpp
 *
 *  Created on: Jun 2, 2015
 *      Author: cs225a-wam
 */

#ifndef OPTITRACKHELPER_HPP
#define OPTITRACKHELPER_HPP

#include "OptiTrack.h"
#include <Eigen/Dense>

namespace SOptiTrackData {
static OptiTrack *objects;
// NOTE TODO : Add all the opti track data here...
static const bool b_useCalibration=false;
static const int num_objects_from_optitrack=3;
static Eigen::Affine3d T_opti_robot;

//Position and rotation from OptiTrack
static Eigen::Vector3d EnvPosOpti;
static Eigen::Matrix3d EnvRotOpti;

static bool flag_optiTrack_running;

static std::string vrpn_server_ip;
}

bool initOptiTrack()
{
    SOptiTrackData::flag_optiTrack_running = false;
    SOptiTrackData::vrpn_server_ip = "172.24.68.48:3883";

    //Initializing vrpn server
    printf("Initializing OptiTrack vrpn_server_ip: %s....", SOptiTrackData::vrpn_server_ip.c_str());
    printf("done\n");

    // Obtain the objects...
    SOptiTrackData::objects= new OptiTrack[SOptiTrackData::num_objects_from_optitrack];
    SOptiTrackData::objects[0].Init(SOptiTrackData::vrpn_server_ip, "Filler");
    SOptiTrackData::objects[1].Init(SOptiTrackData::vrpn_server_ip, "tool"); //Grasped object name
    SOptiTrackData::objects[2].Init(SOptiTrackData::vrpn_server_ip, "env2"); //Env object

    // Rotate the optitrack frame into the robot frame. Assumes that the robot base is
    // in a certain configuration..
    // We will initialize the transform so that we have:
    //    x_opti = T_opti_robot * x_robot;

    SOptiTrackData::T_opti_robot.setIdentity();
    // SOptiTrackData::T_opti_robot.matrix().block(0,0,3,3) << 0,0,-1,-1,0,0,0,1,0;
    // Eigen::Vector3d p_robot_opti; p_robot_opti<<1.10,-0.208,-0.37;
    // SOptiTrackData::T_opti_robot.translation()=SOptiTrackData::T_opti_robot.rotation()*p_robot_opti;
    //SOptiTrackData::T_opti_robot.matrix().block(0,0,3,3) << -0.0320991, -0.0705948, 0.996988, 0.999484, -0.00164848, 0.0320627, -0.000619947, 0.997504, 0.0706113;
    //SOptiTrackData::T_opti_robot.translation() << 0.886945, -0.972617, 0.0678989;
    SOptiTrackData::T_opti_robot.matrix().block(0,0,3,3) << 0, 0, 1, 1, 0, 0, 0, 1, 0;
    SOptiTrackData::T_opti_robot.translation() << -1.002, -0.011, 0.819;
    SOptiTrackData::T_opti_robot.translation() = SOptiTrackData::T_opti_robot.rotation() * SOptiTrackData::T_opti_robot.translation();
    std::cout << SOptiTrackData::T_opti_robot.matrix() << "\n";

    // NOTE TODO : Initialize T_opti_robot right here so we can test the output. We will use it from now on.
    std::cout<<"\n Testing OptiTrack Frame transform. SCL(1,0,0) should be  OptiTrack()+OptiTranslation";
    Eigen::Vector3d x; x<<1,1,1;
    std::cout<<"\n Input : "<<x.transpose();
    std::cout<<"\n Output : "<<(SOptiTrackData::T_opti_robot.rotation() * x).transpose();
    std::cout<<"\n";

    SOptiTrackData::flag_optiTrack_running = true;
    return true;
}

bool runOptiTrackThread()
{
    Eigen::Matrix3d TempRotOpti;
    Eigen::Vector3d TempPosOpti;
    Eigen::Vector3d TempRobotPosOpti;

    while(SOptiTrackData::flag_optiTrack_running)
    {
        double *currentPosition;
        currentPosition = new double[3];
        double **currentRotation;
        currentRotation = new double*[3];
        for (int i = 0; i < 3; i++)
        {
            currentRotation[i] = new double[3];
        }

        //Update Optitrack locations
        for (int k=0; k<SOptiTrackData::num_objects_from_optitrack; k++)
        {
            if (SOptiTrackData::objects[k].IsEnabled())
            {
                //reading new values, if there is any
                SOptiTrackData::objects[k].Update();
                // Getting the object position
                SOptiTrackData::objects[k].GetPosition(currentPosition, SOptiTrackData::b_useCalibration);
                // Getting the object orientation
                SOptiTrackData::objects[k].GetRotationMatrix(currentRotation, SOptiTrackData::b_useCalibration);
                // SOptiTrackData::objects[k].Print();
            }


            if(k==0)
            {
                //FILLER
            }

            if(k==1)
            {
                //TOOL
            }

            if(k==2) //ENV
            {
                for (unsigned int i = 0; i < 3; i++)
                {
                    for (unsigned int j = 0; j < 3; j++)
                    {
                        TempRotOpti(i,j)=currentRotation[i][j];
                    }
                }

                TempPosOpti(0)=currentPosition[0];
                TempPosOpti(1)=currentPosition[1];
                TempPosOpti(2)=currentPosition[2];

                SOptiTrackData::EnvPosOpti=SOptiTrackData::T_opti_robot*TempPosOpti;
                SOptiTrackData::EnvRotOpti=SOptiTrackData::T_opti_robot.rotation()*TempRotOpti;

                //        std::cout<< "\nEnv position : " <<SOptiTrackData::EnvPosOpti.transpose();
                //        std::cout<<"\n Env Rotation : \n" <<SOptiTrackData::EnvRotOpti;

            }
        }
    }
    return true;
}

void exitOptitrack()
{
    delete[] SOptiTrackData::objects;
}

#endif // OPTITRACKHELPER_HPP

