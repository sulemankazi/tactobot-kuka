//  ---------------------- Doxygen info ----------------------
//! \file 09_RMLPositionSampleApplication.cpp
//!
//! \brief
//! Test application number 9 for the Reflexxes Motion Libraries
//! (simplistic algorithm to prevent from overshooting)
//! \n
//! \n
//! \n
//! Reflexxes GmbH\n
//! Sandknoell 7\n
//! D-24805 Hamdorf\n
//! GERMANY\n
//! \n
//! http://www.reflexxes.com\n
//!
//! \date April 2013
//! 
//! \version 1.2.8
//!
//!	\author Torsten Kroeger, <info@reflexxes.com>
//!	
//!
//! \note Copyright (C) 2013 Reflexxes GmbH.
//  ----------------------------------------------------------
//   For a convenient reading of this file's source code,
//   please use a tab width of four characters.
//  ----------------------------------------------------------


#include <stdio.h>
#include <stdlib.h>

#include <ReflexxesAPI.h>
#include <RMLPositionFlags.h>
#include <RMLPositionInputParameters.h>
#include <RMLPositionOutputParameters.h>


//*************************************************************************
// defines

#define CYCLE_TIME_IN_SECONDS					0.001
#define NUMBER_OF_DOFS							3
#define EPSILON									1e-5							



//*************************************************************************
// Main function to run the process that contains the test application
// 
// This function contains source code to get started with the target
// position-based on-line trajectory generation algorithm of the Type IV  
// Reflexxes Motion Library. It demonstrates a simple strategy for
// detecting overshootings. Once an overshooting is detected, the target
// velocity-based algorithm is called with a target velocity vector of
// zero in order to prevent from the overshooting.
//*************************************************************************
int main()
{
    // ********************************************************************
    // Variable declarations and definitions

	bool						AnOVershootingWillHappen	=	false	;
    
    int							ResultValue					=	0		
							,	i							=	0		;

    ReflexxesAPI				*RML						=	NULL	;
    
    RMLPositionInputParameters	*PositionIP					=	NULL	;
    
    RMLPositionOutputParameters	*PositionOP					=	NULL	;

    RMLVelocityInputParameters	*VelocityIP					=	NULL	;
    
    RMLVelocityOutputParameters	*VelocityOP					=	NULL	;
    
    RMLPositionFlags			PositionFlags							;

	RMLVelocityFlags			VelocityFlags							;

    // ********************************************************************
    // Creating all relevant objects of the Type IV Reflexxes Motion Library	
    
    RML			=	new ReflexxesAPI(			NUMBER_OF_DOFS
                                            ,	CYCLE_TIME_IN_SECONDS	);
    
    PositionIP	=	new RMLPositionInputParameters(		NUMBER_OF_DOFS	);
    
	PositionOP	=	new RMLPositionOutputParameters(	NUMBER_OF_DOFS	);

    VelocityIP	=	new RMLVelocityInputParameters(		NUMBER_OF_DOFS	);
    
    VelocityOP	=	new RMLVelocityOutputParameters(	NUMBER_OF_DOFS	);
    
    // ********************************************************************
    // Set-up a timer with a period of one millisecond
    // (not implemented in this example in order to keep it simple)
    // ********************************************************************
    
    printf("-------------------------------------------------------\n"	);
    printf("Reflexxes Motion Libraries                             \n"	);
    printf("Example: 09_RMLPositionSampleApplication               \n\n");
    printf("This example demonstrates the most basic use of the    \n"	);
    printf("Reflexxes API (class ReflexxesAPI) using the position- \n"	);
    printf("based Type IV Online Trajectory Generation algorithm. \n\n"	);
    printf("Copyright (C) 2013 Reflexxes GmbH                      \n"	);
    printf("-------------------------------------------------------\n"	);

    // ********************************************************************
    // Set-up the input parameters
    
    // In this test program, arbitrary values are chosen. If executed on a
    // real robot or mechanical system, the position is read and stored in
    // an RMLPositionInputParameters::CurrentPositionVector vector object.
    // For the very first motion after starting the controller, velocities
    // and acceleration are commonly set to zero. The desired target state
    // of motion and the motion constraints depend on the robot and the
    // current task/application.
    // The internal data structures make use of native C data types
    // (e.g., PositionIP->CurrentPositionVector->VecData is a pointer to
    // an array of NUMBER_OF_DOFS double values), such that the Reflexxes
    // Library can be used in a universal way.
    
    PositionIP->CurrentPositionVector->VecData		[0]	=	 100.0		;
    PositionIP->CurrentPositionVector->VecData		[1]	=	   0.0		;
    PositionIP->CurrentPositionVector->VecData		[2]	=	  50.0		;
    
    PositionIP->CurrentVelocityVector->VecData		[0]	=	 100.0		;
    PositionIP->CurrentVelocityVector->VecData		[1]	=	-220.0		;
    PositionIP->CurrentVelocityVector->VecData		[2]	=	 -50.0		;
    
    PositionIP->CurrentAccelerationVector->VecData	[0]	=	-150.0		;
    PositionIP->CurrentAccelerationVector->VecData	[1]	=	 250.0		;
    PositionIP->CurrentAccelerationVector->VecData	[2]	=	 -50.0		;	

    PositionIP->MaxVelocityVector->VecData			[0]	=	 300.0		;
    PositionIP->MaxVelocityVector->VecData			[1]	=	 100.0		;
    PositionIP->MaxVelocityVector->VecData			[2]	=	 300.0		;
    
    PositionIP->MaxAccelerationVector->VecData		[0]	=	 300.0		;
    PositionIP->MaxAccelerationVector->VecData		[1]	=	 200.0		;
    PositionIP->MaxAccelerationVector->VecData		[2]	=	 100.0		;		

    PositionIP->MaxJerkVector->VecData				[0]	=	 400.0		;
    PositionIP->MaxJerkVector->VecData				[1]	=	 300.0		;
    PositionIP->MaxJerkVector->VecData				[2]	=	 200.0		;
    
    PositionIP->TargetPositionVector->VecData		[0]	=	-600.0		;
    PositionIP->TargetPositionVector->VecData		[1]	=	-200.0		;
    PositionIP->TargetPositionVector->VecData		[2]	=	-350.0		;
    
    PositionIP->TargetVelocityVector->VecData		[0]	=	 50.0		;
    PositionIP->TargetVelocityVector->VecData		[1]	=	-50.0		;
    PositionIP->TargetVelocityVector->VecData		[2]	=  -200.0		;

    PositionIP->SelectionVector->VecData			[0]	=	true		;
    PositionIP->SelectionVector->VecData			[1]	=	true		;
    PositionIP->SelectionVector->VecData			[2]	=	true		;

    // ********************************************************************
    // Starting the control loop
    
    while (ResultValue != ReflexxesAPI::RML_FINAL_STATE_REACHED)
    {
    
        // ****************************************************************									
        // Wait for the next timer tick
        // (not implemented in this example in order to keep it simple)
        // ****************************************************************	
    
        // Calling the Reflexxes OTG algorithm
        ResultValue	=	RML->RMLPosition(		*PositionIP
                                            ,	PositionOP
                                            ,	PositionFlags		);
                                            
        if (ResultValue < 0)
        {
            printf("An error occurred (%d).\n", ResultValue	);
            break;
        }

		if ( ResultValue != ReflexxesAPI::RML_FINAL_STATE_REACHED )
		{		
			AnOVershootingWillHappen	=	false;
			
			for (i = 0; i < NUMBER_OF_DOFS; i++)
			{
				if (PositionIP->TargetVelocityVector->VecData[i] > 0.0)
				{				
					if (	(PositionOP->MaxPosExtremaPositionVectorOnly->VecData[i]	>	PositionIP->TargetPositionVector->VecData[i] + EPSILON	)
						&&	(PositionIP->TargetPositionVector->VecData[i]				>	PositionIP->CurrentPositionVector->VecData[i]			)	)
					{
						AnOVershootingWillHappen	=	true;
						break;
					}
				}
				if (PositionIP->CurrentVelocityVector->VecData[i] < 0.0)
				{				
					if (	(PositionOP->MinPosExtremaPositionVectorOnly->VecData[i]	<	PositionIP->TargetPositionVector->VecData[i] - EPSILON	)
						&&	(PositionIP->TargetPositionVector->VecData[i]				<	PositionIP->CurrentPositionVector->VecData[i]			)	)
					{
						AnOVershootingWillHappen	=	true;
						break;
					}
				}
			}

			// ************************************************************									
			// If the value of AnOVershootingWillHappen is 'true', an
			// overshooting will happen. In this case, we compute a new
			// trajectory using the target velocity-based on-line
			// trajectory generation algorithm. In order to prevent from
			// an overshooting, we can apply different strategies. The
			// following example decelerates all degrees of freedom to
			// zero-velocity. In the next control cycle, the input values
			// should be changed (e.g., the target velocity vector should
			// be changed or the target position vector should be adapted.
			// ************************************************************
			
			if (AnOVershootingWillHappen)
			{
			
				*(VelocityIP->CurrentPositionVector		)	=	*(PositionIP->CurrentPositionVector		);			
				*(VelocityIP->CurrentVelocityVector		)	=	*(PositionIP->CurrentVelocityVector		);
				*(VelocityIP->CurrentAccelerationVector	)	=	*(PositionIP->CurrentAccelerationVector	);
				
				*(VelocityIP->MaxJerkVector				)	=	*(PositionIP->MaxJerkVector				);
				*(VelocityIP->MaxAccelerationVector		)	=	*(PositionIP->MaxAccelerationVector		);
				
				*(VelocityIP->SelectionVector			)	=	*(PositionIP->SelectionVector			);

				VelocityIP->TargetVelocityVector->VecData		[0]	=	0.0		;
				VelocityIP->TargetVelocityVector->VecData		[1]	=	0.0		;
				VelocityIP->TargetVelocityVector->VecData		[2]	=	0.0		;
				
				ResultValue	=	RML->RMLVelocity(		*VelocityIP
													,	VelocityOP
													,	VelocityFlags		);
													
				*(PositionOP->NewPositionVector)		=	*(VelocityOP->NewPositionVector		);
				*(PositionOP->NewVelocityVector)		=	*(VelocityOP->NewVelocityVector		);
				*(PositionOP->NewAccelerationVector)	=	*(VelocityOP->NewAccelerationVector	);
			}
		}
        
        // ****************************************************************									
        // Here, the new state of motion, that is
        //
        // - OP->NewPositionVector		
        // - OP->NewVelocityVector		
        // - OP->NewAccelerationVector
        //
        // can be used as input values for lower level controllers. In the 
        // most simple case, a position controller in actuator space is 
        // used, but the computed state can be applied to many other 
        // controllers (e.g., Cartesian impedance controllers, 
        // operational space controllers).
        // ****************************************************************

        // ****************************************************************
        // Feed the output values of the current control cycle back to 
        // input values of the next control cycle
        
        *PositionIP->CurrentPositionVector		=	*PositionOP->NewPositionVector		;
        *PositionIP->CurrentVelocityVector		=	*PositionOP->NewVelocityVector		;
        *PositionIP->CurrentAccelerationVector	=	*PositionOP->NewAccelerationVector	;
    }

    // ********************************************************************
    // Deleting the objects of the Reflexxes Motion Library end terminating
    // the process
    
    delete	RML			;
    delete	PositionIP	;
    delete	PositionOP	;
    delete	VelocityIP	;
    delete	VelocityOP	;

    exit(EXIT_SUCCESS)	;
}
