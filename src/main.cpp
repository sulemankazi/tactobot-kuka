/**************************************************************************
***   Copyright (c) 2014 S. Mohammad Khansari, Stanford Robotics Lab,   ***
***                      Stanford University, USA                       ***
***************************************************************************
*
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of Stanford University nor the name of the author may
#       be used to endorse or promote products derived from this software without
#       specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY MOHAMMAD KHANSARI ''AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL MOHAMMAD KHANSARI BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
# OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
# LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
* To get latest upadate of the software please visit:
*                          http://cs.stanford.edu/people/khansari
*
* Please send your feedbacks or questions to:
*                           khansari_at_cs.stanford.edu
***************************************************************************/

#include "signal.h"
#include "IIWARobot.h"
#include "OptitrackHelper.hpp"

// Standard Libs
#include <iostream>
#include <stdio.h>
#include <omp.h>
#include <Eigen/Dense>
#include <ReflexxesAPI.h>

using namespace std;

/************************
 ******** GLOBALS *******
 ************************/
bool b_firstRun = true;
bool DEBUG = true;
char ch = '*';

/******* TRAJECTORY GENERATION *******/
double dt=0.001;
int update_period = 1;
int loop_ctr = 0;

// Trajectory intermediate values
Eigen::Vector3d traj_intr_point;
Eigen::VectorXd traj_intr_lam_vec(4);
Eigen::Quaterniond traj_intr_lam;

// Position trajectory
ReflexxesAPI *RML=NULL;
RMLPositionInputParameters *IP=NULL;
RMLPositionOutputParameters *OP=NULL;
RMLPositionFlags Flags;
int dof_pos = 3;

// Insertion trajectory
ReflexxesAPI *RMLinsertion = NULL;
RMLPositionInputParameters *IPinsertion= NULL;
RMLPositionOutputParameters  *OPinsertion = NULL;
RMLPositionFlags Flagsinsertion;
int dof_insertion = 3;

// Orientation trajectory
ReflexxesAPI *RMLori = NULL;
RMLPositionInputParameters *IPori = NULL;
RMLPositionOutputParameters *OPori =NULL;
RMLPositionFlags Flagsori;
int dof_ori = 4;

/****** STATE MACHINE ********/
enum State {FLOAT, MOVE_TO_ENV, MOVE_TO_INS_POINT, HOLD, INSERT};
State robot_state = HOLD;
const double MOVE_TO_ENV_THREHSOLD_ERROR = 0.02;
const double MOVE_TO_INS_POINT_THREHSOLD_ERROR = 0.02;
const double INSERT_THREHSOLD_ERROR = 0.02;
bool state_first_run = true;

/****** DYNAMICS *********/
Eigen::Vector3d CurrentPos; // Current End Effector
Eigen::Matrix3d CurrentRot;
Eigen::Quaterniond CurrentLam;
Eigen::Vector3d TrajectoryGoalPos; // Final Trajectory Goal
Eigen::Matrix3d TrajectoryGoalRot; // Goal
Eigen::Quaterniond TrajectoryGoalLam;
Eigen::Vector3d GoalPos; // Control Loop Iteration Goal
Eigen::Matrix3d GoalRot;
Eigen::Vector3d TaskPos_ee(0.0,0.0,0.14); // Position of the Position Task in end effector frame
Eigen::Vector3d TaskOriPos_ee(0.0,0.0,0.0); // Position of the Orientation Task in end effector frame
Eigen::Affine3d T_0_ee;
Eigen::Matrix3d omega_pos_ee, omega_ori_ee; // For Compliance
Eigen::Matrix3d omega_pos, omega_ori;
Eigen::Vector3d HoldPos;
Eigen::Matrix3d HoldRot;
double k_p = 1500;
double error = 0.0;
double rotation_increment;
int num_rotations = 0;

bool InitTrajectory();
char keyPressed();

IIWA::IIWAMsg goalState;


Eigen::Vector3d OldPos;

class IIWAController: public IIWARobot{
public:
    void Controller(const IIWA::IIWAMsg &currentState, IIWA::IIWAMsg &commandState);
};

void IIWAController::Controller(const IIWA::IIWAMsg &currentState, IIWA::IIWAMsg &commandState){
    // Write your desired command in the variable commandState.
    // You do not need to fill all the variables, just set the ones that you want to change now.
    // Important: Don't FORGET to resize them first.

    // Convert IIWA variables to Eigen variables
    for(int i = 0; i<dof_pos; i++) CurrentPos(i) = currentState.cartPosition[i]/1000.0;
    for(int i=0; i<9; i++) CurrentRot(i/3,i%3) = currentState.cartOrientation[i];
    CurrentLam = CurrentRot;

    T_0_ee.translation() = CurrentPos;
    T_0_ee.matrix().block(0,0,3,3) << CurrentRot;

    if (robot_state == FLOAT) {
        omega_pos_ee << 0, 0, 0, 0, 0, 0, 0, 0, 0;
        omega_ori_ee << 0, 0, 0, 0, 0, 0, 0, 0, 0;
        omega_pos << 0, 0, 0, 0, 0, 0, 0, 0, 0;
        omega_ori << 0, 0, 0, 0, 0, 0, 0, 0, 0;
        GoalPos = CurrentPos;
        GoalRot = CurrentRot;

        char c = keyPressed();
        if(c == '1') {
            robot_state = MOVE_TO_ENV;
            state_first_run = true;
        }
    }
    else if (robot_state == HOLD) {
        if (state_first_run) {
            std::cout << "In Hold State\n" << endl;
            state_first_run = false;
            //            HoldPos = CurrentPos;
            HoldPos << 0.599, 0, 0.678;
            HoldRot = CurrentRot;
        }
        omega_pos_ee << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        omega_ori_ee << 0.01, 0, 0, 0, 0.01, 0, 0, 0, 0.01;
        omega_pos << 0, 0, 0, 0, 0, 0, 0, 0, 0;
        omega_ori << 0, 0, 0, 0, 0, 0, 0, 0, 0;
        GoalPos = HoldPos;
        GoalRot = HoldRot;

        char c = keyPressed();
        if (c != '*')
            std::cout << "Key pressed is " << c << "\n";
        if (c == '\n')
            std::cout << "Key pressed was newline \n";
        if(c == '1') {
            robot_state = MOVE_TO_ENV;
            state_first_run = true;
        } else if (c == '2') {
            robot_state = MOVE_TO_INS_POINT;
            state_first_run = true;
        } else if (c == '3') {
            robot_state = INSERT;
            state_first_run = true;
        } else if (c == '4') {
            robot_state = FLOAT;
            state_first_run = true;
        }
    }
    else if(robot_state == MOVE_TO_ENV) {
        if(state_first_run) {
            std::cout << "Moving to tray\n";
            //             std::cout << "First run\n";
            loop_ctr = 0;
            state_first_run = false;

            // std::cout << "Getting Lambda vector\n";
            Eigen::Vector4d current_lam_vector; // Current Quaternion as a vector
            current_lam_vector(0) = CurrentLam.x();
            current_lam_vector(1) = CurrentLam.y();
            current_lam_vector(2) = CurrentLam.z();
            current_lam_vector(3) = CurrentLam.w();

            // std::cout << "Generating Trajectory\n";
            for(int i = 0; i < dof_pos; i++) IP->CurrentPositionVector->VecData[i] = CurrentPos(i);
            for(int i = 0; i < dof_pos; i++) IP->CurrentVelocityVector->VecData[i] = 0;
            for(int i = 0; i < dof_pos; i++) IP->CurrentAccelerationVector->VecData[i] = 0;

            // std::cout << "OOOriiiiii\n";
            for(int i = 0; i < dof_ori; i++) IPori->CurrentPositionVector->VecData[i] = current_lam_vector(i);
            for(int i = 0; i < dof_ori; i++) IPori->CurrentVelocityVector->VecData[i] = 0;
            for(int i = 0; i < dof_ori; i++) IPori->CurrentAccelerationVector->VecData[i] = 0;

            // std::cout << "Setting selection matrix\n";
            omega_pos_ee << 1, 0, 0, 0, 1, 0, 0, 0, 1;
            omega_ori_ee << 1, 0, 0, 0, 1, 0, 0, 0, 1;
            omega_pos << 1, 0, 0, 0, 1, 0, 0, 0, 1;
            omega_ori << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        }

        // Move directly above env
        if (DEBUG) {
            std::cout << "-------------------------------------------------\n";
            std::cout << "Computing Ultimate Goal Position\n";
        }
        Eigen::Vector3d directly_above_env_offset(0.2,0,0); // 10 cm above env
        TrajectoryGoalPos = SOptiTrackData::EnvPosOpti - (SOptiTrackData::EnvRotOpti * directly_above_env_offset);
        if (DEBUG) {
            std::cout << "Current Position: "<<CurrentPos.transpose()<<"\n";
            std::cout << "Ultimate Goal Position: "<<TrajectoryGoalPos.transpose()<<"\n";
            std::cout << "Tray Position Position: "<<SOptiTrackData::EnvPosOpti.transpose()<<"\n";
        }

        Eigen::Matrix3d _90_offset_y; // Orientation Trajectory
        _90_offset_y << 0,0,1,0,1,0,-1,0,0;
        TrajectoryGoalLam = SOptiTrackData::EnvRotOpti*_90_offset_y; //Only for trajectory generation

        // std::cout << "Maybe update the trajectory\n";
        if(loop_ctr % update_period == 0)
        {
            // std::cout << "Lets update the trajectory\n";
            IP->TargetPositionVector->VecData[0] = TrajectoryGoalPos(0);
            IP->TargetPositionVector->VecData[1] = TrajectoryGoalPos(1);
            IP->TargetPositionVector->VecData[2] = TrajectoryGoalPos(2);

            IP->TargetVelocityVector->VecData[0] = 0.0;
            IP->TargetVelocityVector->VecData[1] = 0.0;
            IP->TargetVelocityVector->VecData[2] = 0.0;

            IPori->TargetPositionVector->VecData[0] = TrajectoryGoalLam.x();
            IPori->TargetPositionVector->VecData[1] = TrajectoryGoalLam.y();
            IPori->TargetPositionVector->VecData[2] = TrajectoryGoalLam.z();
            IPori->TargetPositionVector->VecData[3] = TrajectoryGoalLam.w();

            IPori->TargetVelocityVector->VecData[0] = 0.0;
            IPori->TargetVelocityVector->VecData[1] = 0.0;
            IPori->TargetVelocityVector->VecData[2] = 0.0;
            IPori->TargetVelocityVector->VecData[3] = 0.0;
        } loop_ctr++;

        // std::cout << "Maybe updated the trajectory\n";
        // ResultValue contains status information
        int ResultValue = RML->RMLPosition(*IP, OP, Flags);
        for(int i = 0; i < dof_pos; i++) traj_intr_point(i) = OP->NewPositionVector->VecData[i];

        // ResultValue contains status information orientation
        int ResultValue_ori = RMLori->RMLPosition(*IPori, OPori, Flagsori);
        for(int i = 0; i < dof_ori; i++) traj_intr_lam_vec(i) = OPori->NewPositionVector->VecData[i];

        // IMPORTANT: set the data to some initial values for the current state
        *IP->CurrentPositionVector  = *OP->NewPositionVector;
        *IP->CurrentVelocityVector  = *OP->NewVelocityVector;
        *IP->CurrentAccelerationVector  = *OP->NewAccelerationVector;

        // IMPORTANT: set the data to some initial values for the current state
        *IPori->CurrentPositionVector = *OPori->NewPositionVector;
        *IPori->CurrentVelocityVector = *OPori->NewVelocityVector;
        *IPori->CurrentAccelerationVector = *OPori->NewAccelerationVector;

        traj_intr_lam.x() = traj_intr_lam_vec(0);
        traj_intr_lam.y() = traj_intr_lam_vec(1);
        traj_intr_lam.z() = traj_intr_lam_vec(2);
        traj_intr_lam.w() = traj_intr_lam_vec(3);

        traj_intr_lam.normalize();

        GoalPos = traj_intr_point;
        if (DEBUG) {
            std::cout << "Intermediate Goal Position: "<<GoalPos.transpose()<<"\n";
        }
        GoalRot = traj_intr_lam;

        error=(TrajectoryGoalPos - CurrentPos).norm();

        if (error < MOVE_TO_ENV_THREHSOLD_ERROR)
        {
            OldPos = TrajectoryGoalPos;
            robot_state = HOLD;
            //state_first_run = true;
        }
    }
    else if(robot_state == MOVE_TO_INS_POINT) {
        // Offset in optitrack frame
        Eigen::Vector3d insertion_distance(0.1,0,0);

        if(state_first_run) {
            std::cout << "Moving to tray\n";
            //             std::cout << "First run\n";
            loop_ctr = 0;
            state_first_run = false;

            // std::cout << "Getting Lambda vector\n";
            Eigen::Vector4d current_lam_vector; // Current Quaternion as a vector
            current_lam_vector(0) = CurrentLam.x();
            current_lam_vector(1) = CurrentLam.y();
            current_lam_vector(2) = CurrentLam.z();
            current_lam_vector(3) = CurrentLam.w();

            // std::cout << "Generating Trajectory\n";
            for(int i = 0; i < dof_pos; i++) IP->CurrentPositionVector->VecData[i] = CurrentPos(i);
            for(int i = 0; i < dof_pos; i++) IP->CurrentVelocityVector->VecData[i] = 0;
            for(int i = 0; i < dof_pos; i++) IP->CurrentAccelerationVector->VecData[i] = 0;

            // std::cout << "OOOriiiiii\n";
            for(int i = 0; i < dof_ori; i++) IPori->CurrentPositionVector->VecData[i] = current_lam_vector(i);
            for(int i = 0; i < dof_ori; i++) IPori->CurrentVelocityVector->VecData[i] = 0;
            for(int i = 0; i < dof_ori; i++) IPori->CurrentAccelerationVector->VecData[i] = 0;

            // std::cout << "Setting selection matrix\n";
            omega_pos_ee << 1, 0, 0, 0, 1, 0, 0, 0, 1;
            omega_ori_ee << 1, 0, 0, 0, 1, 0, 0, 0, 1;
            omega_pos << 1, 0, 0, 0, 1, 0, 0, 0, 1;
            omega_ori << 1, 0, 0, 0, 1, 0, 0, 0, 1;
        }

        // Move directly above env
        if (DEBUG) {
            std::cout << "-------------------------------------------------\n";
            std::cout << "Computing Ultimate Goal Position\n";
        }
        TrajectoryGoalPos = SOptiTrackData::EnvPosOpti - (SOptiTrackData::EnvRotOpti * insertion_distance);
        if (DEBUG) {
            std::cout << "Current Position: "<<CurrentPos.transpose()<<"\n";
            std::cout << "Ultimate Goal Position: "<<TrajectoryGoalPos.transpose()<<"\n";
            std::cout << "Tray Position Position: "<<SOptiTrackData::EnvPosOpti.transpose()<<"\n";
        }

        Eigen::Matrix3d _90_offset_y; // Orientation Trajectory
        _90_offset_y << 0,0,1,0,1,0,-1,0,0;
        TrajectoryGoalLam = SOptiTrackData::EnvRotOpti*_90_offset_y; //Only for trajectory generation

        // std::cout << "Maybe update the trajectory\n";
        if(loop_ctr % update_period == 0)
        {
            // std::cout << "Lets update the trajectory\n";
            IP->TargetPositionVector->VecData[0] = TrajectoryGoalPos(0);
            IP->TargetPositionVector->VecData[1] = TrajectoryGoalPos(1);
            IP->TargetPositionVector->VecData[2] = TrajectoryGoalPos(2);

            IP->TargetVelocityVector->VecData[0] = 0.0;
            IP->TargetVelocityVector->VecData[1] = 0.0;
            IP->TargetVelocityVector->VecData[2] = 0.0;

            IPori->TargetPositionVector->VecData[0] = TrajectoryGoalLam.x();
            IPori->TargetPositionVector->VecData[1] = TrajectoryGoalLam.y();
            IPori->TargetPositionVector->VecData[2] = TrajectoryGoalLam.z();
            IPori->TargetPositionVector->VecData[3] = TrajectoryGoalLam.w();

            IPori->TargetVelocityVector->VecData[0] = 0.0;
            IPori->TargetVelocityVector->VecData[1] = 0.0;
            IPori->TargetVelocityVector->VecData[2] = 0.0;
            IPori->TargetVelocityVector->VecData[3] = 0.0;
        } loop_ctr++;

        // std::cout << "Maybe updated the trajectory\n";
        // ResultValue contains status information
        int ResultValue = RML->RMLPosition(*IP, OP, Flags);
        for(int i = 0; i < dof_pos; i++) traj_intr_point(i) = OP->NewPositionVector->VecData[i];

        // ResultValue contains status information orientation
        int ResultValue_ori = RMLori->RMLPosition(*IPori, OPori, Flagsori);
        for(int i = 0; i < dof_ori; i++) traj_intr_lam_vec(i) = OPori->NewPositionVector->VecData[i];

        // IMPORTANT: set the data to some initial values for the current state
        *IP->CurrentPositionVector  = *OP->NewPositionVector;
        *IP->CurrentVelocityVector  = *OP->NewVelocityVector;
        *IP->CurrentAccelerationVector  = *OP->NewAccelerationVector;

        // IMPORTANT: set the data to some initial values for the current state
        *IPori->CurrentPositionVector = *OPori->NewPositionVector;
        *IPori->CurrentVelocityVector = *OPori->NewVelocityVector;
        *IPori->CurrentAccelerationVector = *OPori->NewAccelerationVector;

        traj_intr_lam.x() = traj_intr_lam_vec(0);
        traj_intr_lam.y() = traj_intr_lam_vec(1);
        traj_intr_lam.z() = traj_intr_lam_vec(2);
        traj_intr_lam.w() = traj_intr_lam_vec(3);

        traj_intr_lam.normalize();

        GoalPos = traj_intr_point;
        if (DEBUG) {
            std::cout << "Intermediate Goal Position: "<<GoalPos.transpose()<<"\n";
        }
        GoalRot = traj_intr_lam;

        error=(TrajectoryGoalPos - CurrentPos).norm();

        if (error<MOVE_TO_INS_POINT_THREHSOLD_ERROR) {
            robot_state = HOLD;
            //state_first_run = true;
        }
    }
    else if(robot_state == INSERT) {
        if(state_first_run) {
            num_rotations = 0;
            rotation_increment = 0.00872664626; // 0.5 deg in rad
            state_first_run = false;
        }

        //Selection matrices for compliance
        omega_pos_ee << 0.1,0,0,0,0.1,0,0,0,0.02;
        omega_ori_ee << 0,0,0,0,0,0,0,0,0.5;

        omega_pos = T_0_ee.rotation() * omega_pos_ee * T_0_ee.rotation().transpose();
        omega_ori = T_0_ee.rotation() * omega_ori_ee * T_0_ee.rotation().transpose();

        Eigen::Vector3d insertion_distance(0.05,0,0); // offset in optitrack frame
        GoalPos = SOptiTrackData::EnvPosOpti - SOptiTrackData::EnvRotOpti*insertion_distance;

        // Sinusoidal Motion
        if(num_rotations == 100) {
            rotation_increment = -0.00872664626; // -0.5 deg in rad
        } else if(num_rotations == 300) {
            rotation_increment = 0.00872664626; // -0.5 deg in rad
            num_rotations = 0;
        }
        num_rotations++;

        Eigen::Matrix3d DeltaRot;
        DeltaRot << cos(rotation_increment), sin(rotation_increment), 0, -sin(rotation_increment), cos(rotation_increment), 0, 0, 0, 1;
        GoalRot = CurrentRot * DeltaRot; // TODO: Make sure this works

        if (DEBUG) {
            std::cout<<"\nRotation Matrix:"<<omega_pos;
            std::cout<<"\nRotation Matrix:"<<omega_ori;
        }

        // IMPORTANT: Use omega_pos op

        error = (GoalPos - CurrentPos).norm();

        if (error < INSERT_THREHSOLD_ERROR) {
            robot_state = HOLD;
//            state_first_run = true;
        }
    }

    // The following value are set by the state machine.
    // GoalPos, GoalRot, omega_pos, omega_ori, omega_pos_ee, omega_ori_ee
    // Convert them to IIWA variables

    //You need to set this flag to define the control type
    commandState.isJointControl = false;

    commandState.cartPosition.resize(3);
    for(int i=0; i<3; i++) commandState.cartPosition.at(i) = GoalPos(i)*1000;
    if (DEBUG) {
        std::cout << "Old" << OldPos.transpose() << "\n";
        std::cout << "Sending " << commandState.cartPosition.at(0) <<
                     " " << commandState.cartPosition.at(1) <<
                     " " << commandState.cartPosition.at(2) << "\n";
    }

    commandState.cartOrientation.resize(9);
    for(int i=0; i<9; i++)
        commandState.cartOrientation.at(i) = GoalRot(i/3, i%3);


//    std::cout << "Current " << currentState.cartOrientation.at(0) <<
//                 " " << currentState.cartOrientation.at(1) <<
//                 " " << currentState.cartOrientation.at(2) <<
//                 " " << currentState.cartOrientation.at(3) <<
//                 " " << currentState.cartOrientation.at(4) <<
//                 " " << currentState.cartOrientation.at(5) <<
//                 " " << currentState.cartOrientation.at(6) <<
//                 " " << currentState.cartOrientation.at(7) <<
//                 " " << currentState.cartOrientation.at(8) << "\n";


//    std::cout << "Goal " << commandState.cartOrientation.at(0) <<
//                 " " << commandState.cartOrientation.at(1) <<
//                 " " << commandState.cartOrientation.at(2) <<
//                 " " << commandState.cartOrientation.at(3) <<
//                 " " << commandState.cartOrientation.at(4) <<
//                 " " << commandState.cartOrientation.at(5) <<
//                 " " << commandState.cartOrientation.at(6) <<
//                 " " << commandState.cartOrientation.at(7) <<
//                 " " << commandState.cartOrientation.at(8) << "\n";



    commandState.cartPositionStiffness.resize(3);
    commandState.cartPositionStiffness.at(0) = k_p * omega_pos_ee(0,0);
    commandState.cartPositionStiffness.at(1) = k_p * omega_pos_ee(1,1);
    commandState.cartPositionStiffness.at(2) = k_p * omega_pos_ee(2,2);

    //    commandState.cartOrientationStiffness.resize(3);
    //    commandState.cartOrientationStiffness.at(0) = k_p * omega_ori_ee(0,0);
    //    commandState.cartOrientationStiffness.at(1) = k_p * omega_ori_ee(1,1);
    //    commandState.cartOrientationStiffness.at(2) = k_p * omega_ori_ee(2,2);

}

void sighandle(int signal)
{
    exit(1);
}

int main(int argc, char** argv)
{
    bool flag;

    /* **********************OptiTrack <==> SCL Origin Frame & Rotation******************************* */
    flag = initOptiTrack();
    if(flag == false)
    {
        std::cout<<"\n ERROR : Could not initialize the optitrack system. Exiting\n...\n";
        return 1;
    }

    /******************************TRAJECTORY GENERATION************************************/
    flag = InitTrajectory();
    if(flag == false)
    {
        std::cout<<"\n ERROR : Could not initialize the trajectory generation module. Exiting\n...\n";
        return 1;
    }

    T_0_ee.setIdentity();

    ros::init(argc, argv, "iiwa_core");

    IIWAController myIIWAController;

    signal(SIGINT, sighandle);
    signal(SIGQUIT, sighandle);

    omp_set_num_threads(3);
    int thread_id;
#pragma omp parallel private(thread_id)
    {
        thread_id = omp_get_thread_num();
        if (thread_id == 0) {
            //control loop is here
            while(true)
            {
                myIIWAController.RobotUpdate();
            }
        } else if (thread_id == 1) {
            // Optitrack Thread
            bool optitrack_run_flag;
            optitrack_run_flag = runOptiTrackThread();
        } else if (thread_id == 2) {
            ch = ' ';
            while(true) {
                ch = std::cin.get();
                std::cout << ch << "\n";
                char c = std::cin.get();
            }
        }
    }

    return 0;
}

char keyPressed() {
    if (ch == '*')
        return ch;
    std::cout<<"Character was pressed "<<ch<<"\n";
    char temp_ch = ch;
    ch = '*';
    return temp_ch;
}

bool InitTrajectory()
{
    // Initialize variables
    RML = new ReflexxesAPI(dof_pos, dt);
    IP = new RMLPositionInputParameters(dof_pos);
    OP = new RMLPositionOutputParameters(dof_pos);

    for(int i = 0; i < dof_pos; i++) IP->MaxVelocityVector->VecData[i] = 1.2;
    for(int i = 0; i < dof_pos; i++) IP->MaxAccelerationVector->VecData[i] = 0.5;
    for(int i = 0; i < dof_pos; i++) IP->MaxJerkVector->VecData[i] = 0.2;

    // Can optionally choose to ignore certain directions
    IP->SelectionVector->VecData[0] = true;
    IP->SelectionVector->VecData[1] = true;
    IP->SelectionVector->VecData[2] = true;

    // Initialize variables
    RMLinsertion = new ReflexxesAPI(dof_insertion, dt);
    IPinsertion = new RMLPositionInputParameters(dof_insertion);
    OPinsertion = new RMLPositionOutputParameters(dof_insertion);

    for(int i = 0; i < dof_insertion; i++) IPinsertion->MaxVelocityVector->VecData[i] = .6;
    for(int i = 0; i < dof_insertion; i++) IPinsertion->MaxAccelerationVector->VecData[i] = 0.3;
    for(int i = 0; i < dof_insertion; i++) IPinsertion->MaxJerkVector->VecData[i] = 0.2;

    // Can optionally choose to ignore certain directions
    IPinsertion->SelectionVector->VecData[0]  = true;
    IPinsertion->SelectionVector->VecData[1]  = true;
    IPinsertion->SelectionVector->VecData[2]  = true;

    // Initialize variables
    RMLori = new ReflexxesAPI(dof_ori, dt);
    IPori = new RMLPositionInputParameters(dof_ori);
    OPori = new RMLPositionOutputParameters(dof_ori);

    for(int i = 0; i < dof_ori; i++) IPori->MaxVelocityVector->VecData[i] = .004;
    for(int i = 0; i < dof_ori; i++) IPori->MaxAccelerationVector->VecData[i] = .001;
    for(int i = 0; i < dof_ori; i++) IPori->MaxJerkVector->VecData[i] = 0.001;

    // Can optionally choose to ignore certain directions
    IPori->SelectionVector->VecData[0]  = true;
    IPori->SelectionVector->VecData[1]  = true;
    IPori->SelectionVector->VecData[2]  = true;

    return true;
}
